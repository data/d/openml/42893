# OpenML dataset: acp-lung-cancer

https://www.openml.org/d/42893

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Author: Francesca Grisoni, Claudia S. Neuhaus, Miyabi Hishinuma, Gisela Gabernet, Jan A. Hiss, - Masaaki Kotera, Gisbert Schneider
Source: [UCI](https://archive.ics.uci.edu/ml/datasets/Anticancer+peptides) - 2019
Please cite: [Paper](https://link.springer.com/article/10.1007/s00894-019-4007-6)

Peptides with experimental annotations on their anticancer action on breast and lung cancer cells. This dataset only contains the lung cancer data.

Membranolytic anticancer peptides (ACPs) are drawing increasing attention as potential future therapeutics against cancer, due to their ability to hinder the development of cellular resistance and their potential to overcome common hurdles of chemotherapy, e.g., side effects and cytotoxicity.
This dataset contains information on peptides (annotated for their one-letter amino acid code) and their anticancer activity on breast and lung cancer cell lines. The final training sets contained 949 peptides for Breast cancer and 901 peptides for Lung cancer.

### Attribute Information:

The dataset contains three attributes:
1. Peptide ID
2. One-letter amino-acid sequence
3. Class (active, moderately active, experimental inactive, virtual inactive)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42893) of an [OpenML dataset](https://www.openml.org/d/42893). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42893/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42893/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42893/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

